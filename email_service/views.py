""" Cornice services.
"""
from cornice import Service
from  models.address import Address
from  models.attachment import Attachment
from  models.user import User
from  models.email import Email 
from encoder import JSONEncoder
import json 
import datetime
import pprint
import re


hello = Service(name='hello', path='/', description="Simplest app")
address = Service(name='address', path='/addresses', description="shows list of addresses")
users = Service(name='users', path='/users', description="shows single user identified by id")
user = Service(name='user', path='/user/{email}', description="shows list of users")
attachment = Service(name="attachment", path="/attachments", description= "shows list of attachments")
inbox = Service(name="inbox", path="/user/{email}/inbox", description= "shows user's inbox")
sent = Service(name="sent", path="/user/{email}/sent", description= "shows user's sent items")
compose = Service(name="email", path="/compose", description= "compose email")
_EMAIL={}
_ATTACHMENT=[]
_USER=[]
#
#Helper
#
def valid_user(request):
    try:
        ur = json.loads(request.body)
    except ValueError:
        request.errors.add('body', 'user', 'Not valid JSON')
        return
    # make sure we have the fields we want
    if 'fname' not in ur:
        request.errors.add('body', 'fname', 'Missing first name')
        return
    if 'lname' not in ur:
        request.errors.add('body', 'lname', 'Missing last name')
        return
    if 'email' not in ur:
        request.errors.add('body', 'email', 'Missing email')
        return
    request.validated['ur'] = ur

def valid_email(request):
    try:
        email = json.loads(request.body)
    except ValueError:
        request.errors.add('body', 'email', 'Not valid JSON')
        return
    # make sure we have the fields we want
    if 'sender' not in email:
        request.errors.add('body', 'sender', 'Missing content')
        return
    if 'receiver' not in email:
        request.errors.add('body', 'receiver', 'Missing content')
        return
    if 'subject' not in email:
        request.errors.add('body', 'subject', 'Missing content')
        return
    if 'content' not in email:
        request.errors.add('body', 'content', 'Missing content')
        return
    request.validated['email'] = email

def valid_attachment(request):
    try:
        attach = json.loads(request.body)
    except ValueError:
        request.errors.add('body', 'attachment', 'Not valid JSON')
        return
    # make sure we have the fields we want
    
    request.validated['attach'] = attach
 # 
 #REST Services 
 #
 #User Managment
 #

@users.get()
def get_users(request):
    """Returns a list of all users."""
    usrs = []
    for u in User.objects():
		usrs.append(JSONEncoder().encode(u.__dict__))
		pprint.pprint(usrs)
		#printKeyVals(usrs)
    return {'users': usrs}

@user.get()
def get_user(request):
    """Returns a single user."""
    usr = [] 
    eml= request.matchdict['email']
    for u in User.objects():
	if u.email.lower()== eml.lower() :
		usr .append(JSONEncoder().encode(u.__dict__))
     	return {'user': usr}

@users.post(validators=(valid_user))
def post_users(request):
	values=[]
	usr = User()
	ur = json.loads(request.body)
	_USER.insert(0,request.validated['ur'])
	for u, v in _USER.iteritems():
		if u == 'fname':
			usr.fname=v
		if u == 'lname':
			usr.lname=v 
		if u == 'email':
			usr.email=v 
    
	if u is not None:
		return {"status ": "saved"}
	return {"status","failed"}
#
#Email management
#
@inbox.get()
def get_inbox(request):
    """Returns user's inbox"""
    inbx= [] 
    att= []
    eml= request.matchdict['email']
    for e in Email.objects():
	if e.receiver.lower()== eml.lower() :
		if e.attachment is not None:
			for atch in Attachment.objects:
				if e.attachment.id == atch.id:
					att.append(JSONEncoder().encode(e.attachment.__dict__))
					e.attachment = att			
		inbx.append(JSONEncoder().encode(e.__dict__))
    return {'Inbox': inbx}

@sent.get()
def get_sent(request):
    """Returns user's inbox"""
    snt= [] 
    att= []
    eml= request.matchdict['email']
    for e in Email.objects():
	if e.sender.lower()== eml.lower() :
		if e.attachment is not None:
			for atch in Attachment.objects:
				if e.attachment.id == atch.id:
					att.append(JSONEncoder().encode(e.attachment.__dict__))
					e.attachment = att		
		snt.append(JSONEncoder().encode(e.__dict__))
    return {'Sent': snt}

@attachment.get()
def get_attachment(request):
	attachments = []
	for a in Attachment.objects:
		attachments.append(JSONEncoder().encode(a.__dict__))
	return {'attachments':  attachments}

@compose.post(validators=(valid_email))
def post_email(request):
    """Adds a email"""
    values=[]
    att=[]
    eml = Email()
    attch= Attachment()
    _EMAIL=request.validated['email']
    #_ATTACHMENT= []locateByName(_EMAIL[0],"attachment")
    #for k in _EMAIL:
	#values.insert(0,k) 
    for e, v in _EMAIL.iteritems():
    	if e == 'sender':
    		eml.sender=v
    	if e == 'receiver':
    		eml.receiver=v 
    	if e == 'content':
    		eml.content=v 
    	if e == 'subject':
    		eml.subject=v 
      	if e =='attachment':
      	      for  k,a in v.iteritems():
      	      	if k == 'size':
    			attch.size=a 
		if k == 'ownwe':
    			attch.owner=a
    		if k == 'filetype':
    			attch.filetype=a
    		if k == 'filename':
    			attch.filename=a
    		if k == 'filepath':
    			attch.filepath=a
    	      attch.save()
    eml.attachment= attch
    eml.save()
    if eml is not None:
     	return {"status ": "saved"}
    return {"status","failed"}
