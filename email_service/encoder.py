import json
from bson.objectid import ObjectId
import decimal
import datetime
from time import mktime

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime.datetime):
        	  return int(mktime(o.timetuple()))
        if isinstance(o, decimal.Decimal):
        	   return str(o)
        return json.JSONEncoder.default(self, o)

