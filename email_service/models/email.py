from  mongoengine import * 
from  user import User
from attachment import Attachment

class  Email(DynamicDocument ):
	sender = EmailField() #ReferenceField(User)
	receiver= EmailField()
	attachment = ReferenceField(Attachment)
	content = StringField() 
	subject = StringField() 
	created = DateTimeField()
def compose_email(self, sender,receiver,attachment,
	content,subject,created):
	self.sender=sender
	self.receiver=receiver
	self.attachment=attachment
	self.content=content
	self.subject=subject
	self.created=created

	Email.save()