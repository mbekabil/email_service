from  mongoengine import * 

class  User(DynamicDocument ):
	fname = StringField()
	lname = StringField() 
	email = EmailField() 
	created = DateTimeField()
