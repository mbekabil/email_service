"""Main entry point
"""
from pyramid.config import Configurator
import mongoengine
try:
    # for python 2
    from urlparse import urlparse
except ImportError:
    # for python 3
    from urllib.parse import urlparse


def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.include("cornice","mongoengine" )
    db_url = urlparse(settings['mongo_url'])
    config.registry.db =mongoengine.connect(settings['mongo_db'])
    config.scan("email_service.views")
    return config.make_wsgi_app()
